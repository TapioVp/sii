// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "ammus.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"


class CMundo  
{
public:
	CMundo();
	virtual ~CMundo();	
	void Init();
	void Print(char *mensaje, int x, int y, float r, float g, float b);
		
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	void GestionaConexiones();
	
	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	
	Ammus ammusdejugador1;
	Ammus ammusdejugador2;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos[2];
	int puntos1;
	int puntos2;
	int a;
	int a_stay;
	int pause;
	int b;
	int b_stay;
	int i;	
	int pelaajia;
	char* nimi1;
	char* nimi2;
	int eka;
	Socket server;
	std::vector<Socket> clientes;
	std::vector<std::string> nombres;
	
	int acabar;
	pthread_t thid_hilo_conexiones, thid_hilo_comandos1, thid_hilo_comandos2;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
