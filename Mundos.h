// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"


class CMundo  
{
public:
	CMundo();
	virtual ~CMundo();	
	void Init();
	void Print(char *mensaje, int x, int y, float r, float g, float b);
		
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	Ammus ammusdejugador1;
	Ammus ammusdejugador2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Socket servidor;
	void GestionaConexiones();
	std::vector<Socket> conexiones;
	std::vector<std::string> nombres;

	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	int acabar;
	int puntos1;
	int puntos2;
	int a;
	int a_stay;
	
	int b;
	int b_stay;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
