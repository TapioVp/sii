
// Ammus.cpp: implementation of the Ammus class.
//
//////////////////////////////////////////////////////////////////////
#include "ammus.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Ammus::Ammus()
{
	radio=0.2f;
	velocidad.x=4;
	velocidad.y=4;
    ajastin=110; //init freeze timer not to activate
    centro.x=255;
}

Ammus::~Ammus()
{

}



void Ammus::Dibuja()
{
	glColor3ub(155,155,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Ammus::Mueve(float t)
{
	centro=centro+(velocidad*t);
}

