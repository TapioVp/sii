
// Ammus.h: interface for the Ammus (disparo) class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMMUS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_AMMUS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Ammus
{
public:
	Ammus();
	virtual ~Ammus() ;

	Vector2D centro;
	Vector2D velocidad;
	float radio;
	int pienennys; //variable for the size change of raqueta
	int ajastin; //variable for the freeze timer
	int osui; //variable for hit
	int pallohallussa; //variable for owning the ammus
	void Mueve(float t);
	void Dibuja();
};

#endif // !defined(AFX_AMMUS_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
