// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void *hilo_conexiones(void *d)
{
	CMundo *p = (CMundo *) d;
	p->GestionaConexiones();
}

void CMundo::GestionaConexiones()
{
	while(1)
	{
		char cad[100];
	
		client.Receive(cad,100);
		printf("%s",cad);
		nombres.push_back(cad);
	}
} 

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundo::Print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	//sprintf(cad,"Cliente");
	//Print(cad,300,10,1,0,1);
   sprintf(cad," %s : %d \t\t %s : %d", &nimivasen, puntos[0], &nimioik, puntos[1]);
	Print(cad,10,0,1,1,1);

if(ammusdejugador1.ajastin <= 100) {  sprintf(cad,"FREEZE TIMER J2: %i",100-ammusdejugador1.ajastin);	Print(cad,400,0,1,1,1); }
if(ammusdejugador2.ajastin <= 100) {  sprintf(cad,"FREEZE TIMER J1: %i",100-ammusdejugador2.ajastin);	Print(cad,200,0,1,1,1); }
if(pause == 1) { Print("PAUSED",400,0,1,1,1); }
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	ammusdejugador1.Dibuja();
	ammusdejugador2.Dibuja();
	
	

	for(int i= 0; i<nombres.size(); i++)
	{
	if (i<2){
			sprintf(cad, "%s %d", nombres[i].data(), puntos[i]);
			Print(cad,50,50+20*i,1,0,1);
			}
	else
	{
			sprintf(cad, "%s", nombres[i].data());
			Print(cad,50,50+20*i,1,1,1);
	}
		
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char cad[400];	
	client.Receive(cad,400);
sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %i %i %i %i %s %s %d %d", &esfera.centro.x, &esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,
&jugador2.y1,&jugador2.x2,&jugador2.y2,&ammusdejugador1.centro.x,&ammusdejugador1.velocidad.y,
&ammusdejugador2.centro.x,&ammusdejugador2.velocidad.y,&ammusdejugador1.centro.y,
&ammusdejugador2.centro.y,&ammusdejugador1.velocidad.x,&ammusdejugador2.velocidad.x,
&ammusdejugador1.ajastin,&ammusdejugador2.ajastin,&pause,&pelaajia,&nimivasen,&nimioik,
&puntos[0], &puntos[1]);
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[100];
	sprintf(cad,"%c",key);
	client.Send(cad,strlen(cad)+1);
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}*/
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	char nombre [100];
	printf("Introduce tu nombre: \n");
	
	scanf("%s", nombre);
	
	
	client.Connect ("127.0.0.1", 12000);
	
	
	
	client.Send(nombre, strlen(nombre)+1);
	
	//pthread_create(&thid_hilo_conexiones, NULL, hilo_conexiones, this);
}
