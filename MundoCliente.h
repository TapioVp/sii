// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_
#include "ammus.h"
#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"

class CMundo  
{
public:
	CMundo();
	virtual ~CMundo();	
	void Init();
	void Print(char *mensaje, int x, int y, float r, float g, float b);
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	void GestionaConexiones();

	Esfera esfera;

	Ammus ammusdejugador1;
	Ammus ammusdejugador2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int loppu;
	int loppu2;
	char nimioik;
	char nimivasen;
	int pause;
	int pelaajia;
	int puntos[2];
	Socket client;
	
	std::vector<std::string> nombres;
	
	pthread_t thid_hilo_conexiones;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
