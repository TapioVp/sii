CC=g++
CFLAGS= -g -fpermissive
LIBS= -lm -lglut -lGL -lGLU -lpthread
OBJS=Esfera.o Plano.o Raqueta.o Vector2D.o Socket.o ammus.o
HEADERS=Esfera.h MundoCliente.h MundoServidor.h Plano.h Raqueta.h Vector2D.h ammus.h

all: servidor cliente

servidor: $(OBJS) MundoServidor.o servidor.o
	$(CC) $(CFLAGS) -o servidor servidor.o MundoServidor.o $(OBJS) $(LIBS)

cliente: $(OBJS) MundoCliente.o cliente.o
	$(CC) $(CFLAGS) -o cliente cliente.o MundoCliente.o $(OBJS) $(LIBS)

Socket.o: Socket.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c Socket.cpp
MundoCliente.o: MundoCliente.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c MundoCliente.cpp
MundoServidor.o: MundoServidor.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c MundoServidor.cpp
Esfera.o: Esfera.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c Esfera.cpp
Plano.o: Plano.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c Plano.cpp
ammus.o: ammus.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c ammus.cpp
Raqueta.o: Raqueta.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c Raqueta.cpp
Vector2D.o: Vector2D.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c Vector2D.cpp
servidor.o: servidor.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c servidor.cpp
cliente.o: cliente.cpp $(HEADERS)
	$(CC) $(CFLAGS) -c cliente.cpp

clean:
	rm -f *.o cliente servidor
